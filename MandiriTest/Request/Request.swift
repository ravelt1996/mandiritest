//
//  Request.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 12/06/23.
//

import Foundation
import Alamofire
import SwiftyJSON

class Request {
    static let shared = Request()
    
    func fetchGenre(completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void) {
        let url = "https://api.themoviedb.org/3/genre/movie/list"
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(Constant.shared.accessToken)"
        ]
        AF.request(url, method: .get
            , parameters: nil, encoding: URLEncoding.httpBody
            , headers: headers).responseJSON{(response) in
                if(response.response?.statusCode==200){
                    do {
                        let json = try JSON(response.result.get())
                        completionHandler(json, response.error)
                    } catch {
                        return
                    }
                }
        }
    }
    
    func fetchFilmByGenre(page: Int, genre_id: Int, completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void) {
        let url = "https://api.themoviedb.org/3/discover/movie?page=\(page)&with_genres=\(genre_id)"
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(Constant.shared.accessToken)"
        ]
        AF.request(url, method: .get
            , parameters: nil, encoding: URLEncoding.httpBody
            , headers: headers).responseJSON{(response) in
                if(response.response?.statusCode==200){
                    do {
                        let json = try JSON(response.result.get())
                        print(json)
                        completionHandler(json, response.error)
                    } catch {
                        return
                    }
                }
        }
    }
    
    func fetchReviewByFilm(page: Int, film: Film, completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void) {
        let url = "https://api.themoviedb.org/3/movie/\(film.id)/reviews?page=\(page)"
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(Constant.shared.accessToken)"
        ]
        AF.request(url, method: .get
            , parameters: nil, encoding: URLEncoding.httpBody
            , headers: headers).responseJSON{(response) in
                if(response.response?.statusCode==200){
                    do {
                        let json = try JSON(response.result.get())
                        print(json)
                        completionHandler(json, response.error)
                    } catch {
                        return
                    }
                }
        }
    }
    
    func fetchTrailerData(film: Film, completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void) {
        let url = "https://api.themoviedb.org/3/movie/\(film.id)/videos"
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(Constant.shared.accessToken)"
        ]
        AF.request(url, method: .get
            , parameters: nil, encoding: URLEncoding.httpBody
            , headers: headers).responseJSON{(response) in
                if(response.response?.statusCode==200){
                    do {
                        let json = try JSON(response.result.get())
                        print(json)
                        completionHandler(json, response.error)
                    } catch {
                        return
                    }
                }
        }
    }
}
