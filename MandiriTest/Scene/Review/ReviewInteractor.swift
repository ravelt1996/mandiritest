//
//  ReviewInteractor.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import Foundation
import SwiftyJSON
class ReviewInteractor: PresenterToInteractorReviewProtocol {
    var arrReview: [Review] = []
    
    func getReviewData(film: Film, page: Int) {
        Request.shared.fetchReviewByFilm(page: page, film: film, completionHandler: {
            [weak self] result, error in
            if let arrReview = result["results"].array {
                self?.parseReviewByFilm(arrReview: arrReview, page: page)
            }
        })
    }
    
    func parseReviewByFilm(arrReview: [JSON], page: Int) {
        if page<=1 {
            self.arrReview.removeAll()
        }
        
        for item in arrReview {
            let review = Review.init(url: item["url"].string ?? "", author: item["author"].string ?? "", content: item["content"].string ?? "", created_at: item["created_at"].string ?? "")
            self.arrReview.append(review)
        }
        presenter?.updateReviewData(arrReview: self.arrReview)
        
    }
    // MARK: Properties
    var presenter: InteractorToPresenterReviewProtocol?
}
