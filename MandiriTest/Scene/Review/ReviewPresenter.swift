//
//  ReviewPresenter.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import Foundation

class ReviewPresenter: ViewToPresenterReviewProtocol {
    func getReviewData(film: Film?, page: Int) {
        guard let film = film else { return }
        interactor?.getReviewData(film: film, page: page)
    }
    

    // MARK: Properties
    var view: PresenterToViewReviewProtocol?
    var interactor: PresenterToInteractorReviewProtocol?
    var router: PresenterToRouterReviewProtocol?
}

extension ReviewPresenter: InteractorToPresenterReviewProtocol {
    func updateReviewData(arrReview: [Review]) {
        view?.updateReviewByFilm(arrReview: arrReview)
    }
}
