//
//  ReviewViewController.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import UIKit

class ReviewViewController: UIViewController {
    
    // MARK: - Lifecycle Methods
    var film: Film?
    var arrReview: [Review] = []
    var presenter: ViewToPresenterReviewProtocol?
    var page: Int = 1
    var reviewCount: Int = 0
    
    @IBOutlet weak var reviewTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupPresenter()
        setupView()
    }
    
    func setupPresenter() {
        let presenter: ViewToPresenterReviewProtocol & InteractorToPresenterReviewProtocol = ReviewPresenter()
        
        self.presenter = presenter
        self.presenter?.router = ReviewRouter()
        self.presenter?.view = self
        self.presenter?.interactor = ReviewInteractor()
        self.presenter?.interactor?.presenter = presenter
    }
    
    func setupView() {
        reviewTableView.delegate = self
        reviewTableView.dataSource = self
        reviewTableView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")

        presenter?.getReviewData(film: film, page: 1)
    }
}

extension ReviewViewController: PresenterToViewReviewProtocol{
    func updateReviewByFilm(arrReview: [Review]) {
        self.arrReview = arrReview
        if arrReview.count != self.reviewCount {
            reviewTableView.reloadData()
            self.reviewCount = arrReview.count
        }
    }
    
    // TODO: Implement View Output Methods
}

extension ReviewViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReview.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        if let cell = reviewTableView.dequeueReusableCell(withIdentifier: "cell") as? ReviewTableViewCell {
            cell.setupView(review: arrReview[index])
            
            if index == arrReview.count - 1 {
                self.page+=1
                self.presenter?.getReviewData(film: film, page: page)
            }
            return cell
        } else {
            return UITableViewCell()
        }
        
    }
    
    
}
