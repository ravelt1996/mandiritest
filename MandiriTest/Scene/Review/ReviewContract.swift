//
//  ReviewContract.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import Foundation


// MARK: View Output (Presenter -> View)
protocol PresenterToViewReviewProtocol {
    func updateReviewByFilm(arrReview: [Review])
}


// MARK: View Input (View -> Presenter)
protocol ViewToPresenterReviewProtocol {
    
    var view: PresenterToViewReviewProtocol? { get set }
    var interactor: PresenterToInteractorReviewProtocol? { get set }
    var router: PresenterToRouterReviewProtocol? { get set }
    
    func getReviewData(film: Film?, page: Int)
}


// MARK: Interactor Input (Presenter -> Interactor)
protocol PresenterToInteractorReviewProtocol {
    
    var presenter: InteractorToPresenterReviewProtocol? { get set }
    
    func getReviewData(film: Film, page: Int)
}


// MARK: Interactor Output (Interactor -> Presenter)
protocol InteractorToPresenterReviewProtocol {
    func updateReviewData(arrReview: [Review])
}


// MARK: Router Input (Presenter -> Router)
protocol PresenterToRouterReviewProtocol {
    
}
