//
//  ReviewTableViewCell.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var createdAt: UILabel!
    @IBOutlet weak var authorName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupView(review: Review) {
        contentLabel.text = review.content
        createdAt.text = review.created_at
        authorName.text = review.author
    }
    
}
