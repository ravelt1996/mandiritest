//
//  FilmByGenreViewController.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import UIKit

class FilmByGenreViewController: UIViewController {
    
    @IBOutlet weak var filmTableView: UITableView!
    var genre: Genre?
    var arrFilm: [Film] = []
    var page: Int = 1
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupPresenter()
        setupView()
    }
    
    func setupPresenter() {
        let presenter: ViewToPresenterFilmByGenreProtocol & InteractorToPresenterFilmByGenreProtocol = FilmByGenrePresenter()
        
        self.presenter = presenter
        self.presenter?.router = FilmByGenreRouter()
        self.presenter?.view = self
        self.presenter?.interactor = FilmByGenreInteractor()
        self.presenter?.interactor?.presenter = presenter
    }
    
    func setupView() {
        filmTableView.dataSource = self
        filmTableView.delegate = self
        self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(
            title: "Back", style: .plain, target: nil, action: nil)
        guard let genre = self.genre else { return }
        self.title = "Genre \(genre.name)"
        presenter?.getFilmByGenre(genre: genre, page: page)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //deinitFiles()
    }
    
    func deinitFiles() {
        self.presenter?.router = nil
        self.presenter?.interactor?.presenter = nil
        self.presenter?.interactor = nil
        self.presenter = nil
    }
    
    deinit {
        print("FilmByGenreViewController deallocated")
    }
    
    // MARK: - Properties
    var presenter: ViewToPresenterFilmByGenreProtocol?
    
}

extension FilmByGenreViewController: PresenterToViewFilmByGenreProtocol{
    func updateFilmByGenre(arrFilm: [Film]) {
        self.arrFilm = arrFilm
        filmTableView.reloadData()
    }
    
    // TODO: Implement View Output Methods
}

extension FilmByGenreViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilm.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let cell = UITableViewCell()
        cell.textLabel?.text = arrFilm[index].title
        if indexPath.row == arrFilm.count - 1 { // last cell
            page+=1
            if let genre = self.genre {
                self.presenter?.getFilmByGenre(genre: genre, page: page)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        let film = arrFilm[index]
        self.presenter?.goToDetailFilm(film: film, nav: self.navigationController)
    }
    
    
}
