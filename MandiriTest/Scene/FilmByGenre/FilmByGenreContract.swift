//
//  FilmByGenreContract.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import Foundation
import UIKit


// MARK: View Output (Presenter -> View)
protocol PresenterToViewFilmByGenreProtocol {
    func updateFilmByGenre(arrFilm: [Film])
}


// MARK: View Input (View -> Presenter)
protocol ViewToPresenterFilmByGenreProtocol {
    
    var view: PresenterToViewFilmByGenreProtocol? { get set }
    var interactor: PresenterToInteractorFilmByGenreProtocol? { get set }
    var router: PresenterToRouterFilmByGenreProtocol? { get set }
    func goToDetailFilm(film: Film, nav: UINavigationController?)
    func getFilmByGenre(genre: Genre, page: Int)
}


// MARK: Interactor Input (Presenter -> Interactor)
protocol PresenterToInteractorFilmByGenreProtocol {
    
    var presenter: InteractorToPresenterFilmByGenreProtocol? { get set }
    func getFilmByGenre(genre: Genre, page: Int)
}


// MARK: Interactor Output (Interactor -> Presenter)
protocol InteractorToPresenterFilmByGenreProtocol {
    func presentFilmByGenre(arrFilm: [Film])
}


// MARK: Router Input (Presenter -> Router)
protocol PresenterToRouterFilmByGenreProtocol {
    func goToDetailFilm(film: Film, nav: UINavigationController?)
}
