//
//  FilmByGenreInteractor.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import Foundation
import SwiftyJSON

class FilmByGenreInteractor: PresenterToInteractorFilmByGenreProtocol {

    // MARK: Properties
    var presenter: InteractorToPresenterFilmByGenreProtocol?
    var arrFilm: [Film] = []
    
    func getFilmByGenre(genre: Genre, page: Int) {
        Request.shared.fetchFilmByGenre(page: page, genre_id: genre.id, completionHandler: {
            [weak self] result,error in
            if let arrFilm = result["results"].array {
                self?.parseFilmByGenre(array: arrFilm, page: page)
            }
        })
    }
    
    func parseFilmByGenre(array: [JSON], page: Int) {
        if page <= 1 {
            arrFilm.removeAll()
        }
        for item in array {
            arrFilm.append(parseAllData(data: item))
        }
        presenter?.presentFilmByGenre(arrFilm: arrFilm)
    }
    
    func parseAllData(data: JSON) -> Film {
        let film = Film.init(title: data["title"].string ?? "", vote_average: data["vote_average"].float ?? 0.0, id: data["id"].int ?? 0, original_title: data["original_title"].string ?? "", video: data["video"].bool ?? false, popularity: data["popularity"].float ?? 0.0, poster_path: data["poster_path"].string ?? "", backdrop_path: data["backdrop_path"].string ?? "", vote_count: data["vote_count"].int ?? 0, original_language: data["original_language"].string ?? "", overview: data["overview"].string ?? "", release_date: data["release_date"].string ?? "")
        return film
    }
}
