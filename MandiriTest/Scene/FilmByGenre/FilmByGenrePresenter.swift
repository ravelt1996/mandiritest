//
//  FilmByGenrePresenter.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import Foundation
import UIKit

class FilmByGenrePresenter: ViewToPresenterFilmByGenreProtocol {
    func goToDetailFilm(film: Film, nav: UINavigationController?) {
        router?.goToDetailFilm(film: film, nav: nav)
    }
    

    // MARK: Properties
    var view: PresenterToViewFilmByGenreProtocol?
    var interactor: PresenterToInteractorFilmByGenreProtocol?
    var router: PresenterToRouterFilmByGenreProtocol?
    
    func getFilmByGenre(genre: Genre, page: Int) {
        interactor?.getFilmByGenre(genre: genre, page: page)
    }
    

}

extension FilmByGenrePresenter: InteractorToPresenterFilmByGenreProtocol {
    func presentFilmByGenre(arrFilm: [Film]) {
        view?.updateFilmByGenre(arrFilm: arrFilm)
    }
}
