//
//  HomeContract.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 12/06/23.
//  
//

import Foundation
import UIKit


// MARK: View Output (Presenter -> View)
protocol PresenterToViewHomeProtocol {
    func loadGenreData(arrGenre: [Genre])
}


// MARK: View Input (View -> Presenter)
protocol ViewToPresenterHomeProtocol {
    
    var view: PresenterToViewHomeProtocol? { get set }
    var interactor: PresenterToInteractorHomeProtocol? { get set }
    var router: PresenterToRouterHomeProtocol? { get set }
    func goToFilmByGenre(genre: Genre, nav: UINavigationController?)
    func getGenreData()	
}


// MARK: Interactor Input (Presenter -> Interactor)
protocol PresenterToInteractorHomeProtocol {
    
    var presenter: InteractorToPresenterHomeProtocol? { get set }
    func fetchGenreData()
}


// MARK: Interactor Output (Interactor -> Presenter)
protocol InteractorToPresenterHomeProtocol {
    func presentGenreData(arrGenre: [Genre])
}


// MARK: Router Input (Presenter -> Router)
protocol PresenterToRouterHomeProtocol {
    func goToFilmByGenre(genre: Genre, nav: UINavigationController?)
}
