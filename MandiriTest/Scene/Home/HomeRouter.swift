//
//  HomeRouter.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 12/06/23.
//  
//

import Foundation
import UIKit

class HomeRouter: PresenterToRouterHomeProtocol {
    func goToFilmByGenre(genre: Genre, nav: UINavigationController?) {
        if let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilmByGenreViewController") as? FilmByGenreViewController {
            vc.genre = genre
            nav?.pushViewController(vc, animated: true)
        } else {
            return
        }
    }
    
}
