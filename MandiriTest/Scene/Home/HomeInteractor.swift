//
//  HomeInteractor.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 12/06/23.
//  
//

import Foundation
import SwiftyJSON

class HomeInteractor: PresenterToInteractorHomeProtocol {
    // MARK: Properties
    var presenter: InteractorToPresenterHomeProtocol?
    var arrGenre: [Genre] = []
    
    func fetchGenreData() {
        Request.shared.fetchGenre(completionHandler: {
            [weak self] json,error in
            if let genres = json["genres"].array {
                self?.parseGenreData(genres)
            } else {
                return
            }
        })
    }
    
    func parseGenreData(_ arrayData: [JSON]) {
        arrGenre.removeAll()
        for item in arrayData {
            if let name = item["name"].string, let id = item["id"].int {
                let genre = Genre.init(name: name, id: id)
                arrGenre.append(genre)
            }
        }
        presenter?.presentGenreData(arrGenre: arrGenre)
    }
}
