//
//  HomeViewController.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 12/06/23.
//  
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var genreTableView: UITableView!
    var arrGenre: [Genre] = []
    var presenter: ViewToPresenterHomeProtocol?
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupPresenter()
        setupView()
    }
    
    func setupPresenter() {
        let presenter: ViewToPresenterHomeProtocol & InteractorToPresenterHomeProtocol = HomePresenter()
        
        self.presenter = presenter
        self.presenter?.router = HomeRouter()
        self.presenter?.view = self
        self.presenter?.interactor = HomeInteractor()
        self.presenter?.interactor?.presenter = presenter
    }
    
    func setupView() {
        genreTableView.delegate = self
        genreTableView.dataSource = self
        presenter?.getGenreData()
    }
    
}

extension HomeViewController: PresenterToViewHomeProtocol{
    func loadGenreData(arrGenre: [Genre]) {
        self.arrGenre = arrGenre
        self.genreTableView.reloadData()
    }
    // TODO: Implement View Output Methods
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrGenre.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let cell = UITableViewCell()
        cell.textLabel?.text = arrGenre[index].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let genre = arrGenre[indexPath.row]
        presenter?.goToFilmByGenre(genre: genre, nav: self.navigationController)
    }
}
