//
//  HomePresenter.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 12/06/23.
//  
//

import Foundation
import UIKit

class HomePresenter: ViewToPresenterHomeProtocol {
    func goToFilmByGenre(genre: Genre, nav: UINavigationController?) {
        router?.goToFilmByGenre(genre: genre, nav: nav)
    }
    
    func getGenreData() {
        interactor?.fetchGenreData()
    }

    // MARK: Properties
    var view: PresenterToViewHomeProtocol?
    var interactor: PresenterToInteractorHomeProtocol?
    var router: PresenterToRouterHomeProtocol?
}

extension HomePresenter: InteractorToPresenterHomeProtocol {
    func presentGenreData(arrGenre: [Genre]) {
        view?.loadGenreData(arrGenre: arrGenre)
    }
}
