//
//  FilmDetailViewController.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import UIKit
import Kingfisher
import SkeletonView

class FilmDetailViewController: UIViewController {
    
    var film: Film?
    var trailerURL: URL?
    @IBOutlet weak var filmLabel: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var popularityLabel: UILabel!
    @IBOutlet weak var originLanguageLabel: UILabel!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var btnWatchTrailer: UIButton!
    @IBOutlet weak var posterImage: UIImageView!
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupPresenter()
        setupView()
    }

    func setupPresenter() {
        let presenter: ViewToPresenterFilmDetailProtocol & InteractorToPresenterFilmDetailProtocol = FilmDetailPresenter()
        
        self.presenter = presenter
        self.presenter?.router = FilmDetailRouter()
        self.presenter?.view = self
        self.presenter?.interactor = FilmDetailInteractor()
        self.presenter?.interactor?.presenter = presenter
    }
    
    func setupView() {
        btnWatchTrailer.isHidden = true
        guard let film = self.film else { return }
        presenter?.showFilmDetail(film: film)
        presenter?.getTrailerData(film: film)
        
    }
    @IBAction func reviewTapped(_ sender: UIButton) {
        guard let film = self.film else { return }
        presenter?.goToReview(film: film, nav: self.navigationController)
    }
    // MARK: - Properties
    var presenter: ViewToPresenterFilmDetailProtocol?
    
    @IBAction func trailerTapped(_ sender: UIButton) {
        if let url = trailerURL {
            presenter?.watchTrailer(url: url)
        }
    }
}

extension FilmDetailViewController: PresenterToViewFilmDetailProtocol{
    func updateYoutubeURL(url: URL) {
        trailerURL = url
        btnWatchTrailer.isHidden = false
    }
    
    func updateFilmView(film: Film) {
        filmLabel.text = film.title
        releaseDate.text = film.release_date
        rateLabel.text = "Vote Average : \(film.vote_average)"
        voteCountLabel.text = "Vote Count : \(film.vote_count)"
        originLanguageLabel.text = film.original_language
        popularityLabel.text = "Popularity : \(film.popularity)"
        overviewLabel.text = film.overview
        posterImage.isSkeletonable = true
        posterImage.showGradientSkeleton()
        if let url = URL(string: film.poster_path ?? "") {
            KF.url(url)
              .loadDiskFileSynchronously()
              .cacheMemoryOnly()
              .fade(duration: 0.25)
              .onSuccess { [weak self] result in
                  self?.posterImage.hideSkeleton()
              }
              .set(to: posterImage)
        }
        
    }
    
    func watchTrailer(url: URL) {
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //If you want handle the completion block than
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                 print("Open url : \(success)")
            })
        }
    }
    
    // TODO: Implement View Output Methods
}
