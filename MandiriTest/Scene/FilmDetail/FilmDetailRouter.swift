//
//  FilmDetailRouter.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import Foundation
import UIKit

class FilmDetailRouter: PresenterToRouterFilmDetailProtocol {
    func goToReview(film: Film, nav: UINavigationController?) {
        if let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReviewViewController") as? ReviewViewController {
            vc.film = film
            nav?.pushViewController(vc, animated: true)
        } else {
            return
        }
    }
    
    
}
