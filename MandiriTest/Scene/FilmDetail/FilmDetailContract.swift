//
//  FilmDetailContract.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import Foundation
import UIKit


// MARK: View Output (Presenter -> View)
protocol PresenterToViewFilmDetailProtocol {
    func updateFilmView(film: Film)
    func updateYoutubeURL(url: URL)
    func watchTrailer(url: URL)
}


// MARK: View Input (View -> Presenter)
protocol ViewToPresenterFilmDetailProtocol {
    
    var view: PresenterToViewFilmDetailProtocol? { get set }
    var interactor: PresenterToInteractorFilmDetailProtocol? { get set }
    var router: PresenterToRouterFilmDetailProtocol? { get set }
    
    func showFilmDetail(film: Film)
    func goToReview(film: Film, nav: UINavigationController?)
    func getTrailerData(film: Film)
    func watchTrailer(url: URL)
}


// MARK: Interactor Input (Presenter -> Interactor)
protocol PresenterToInteractorFilmDetailProtocol {
    
    var presenter: InteractorToPresenterFilmDetailProtocol? { get set }
    func getTrailerData(film: Film)
}


// MARK: Interactor Output (Interactor -> Presenter)
protocol InteractorToPresenterFilmDetailProtocol {
    func getTrailerData(film: Film)
    func updateYoutubeURL(url: URL)
}


// MARK: Router Input (Presenter -> Router)
protocol PresenterToRouterFilmDetailProtocol {
    func goToReview(film: Film, nav: UINavigationController?)
}
