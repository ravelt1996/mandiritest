//
//  FilmDetailPresenter.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import Foundation
import UIKit

class FilmDetailPresenter: ViewToPresenterFilmDetailProtocol {
    func getTrailerData(film: Film) {
        interactor?.getTrailerData(film: film)
    }
    
    func goToReview(film: Film, nav: UINavigationController?) {
        router?.goToReview(film: film, nav: nav)
    }
    
    func showFilmDetail(film: Film) {
        view?.updateFilmView(film: film)
    }
    
    func watchTrailer(url: URL) {
        view?.watchTrailer(url: url)
    }

    // MARK: Properties
    var view: PresenterToViewFilmDetailProtocol?
    var interactor: PresenterToInteractorFilmDetailProtocol?
    var router: PresenterToRouterFilmDetailProtocol?
}

extension FilmDetailPresenter: InteractorToPresenterFilmDetailProtocol {
    func updateYoutubeURL(url: URL) {
        view?.updateYoutubeURL(url: url)
    }
}
