//
//  FilmDetailInteractor.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//  
//

import Foundation
import SwiftyJSON

class FilmDetailInteractor: PresenterToInteractorFilmDetailProtocol {
    // MARK: Properties
    var presenter: InteractorToPresenterFilmDetailProtocol?
    func getTrailerData(film: Film) {
        Request.shared.fetchTrailerData(film: film, completionHandler: { [weak self] result,error in
            if let arrYoutube = result["results"].array {
                self?.parseYoutubeKey(arrayResult: arrYoutube)
            }
        })
    }
    
    func parseYoutubeKey(arrayResult: [JSON]) {
        var youtubeKey = ""
        if arrayResult.count >= 1 {
            youtubeKey = arrayResult[0]["key"].string ?? ""
        }
        if let url = URL(string: "https://www.youtube.com/watch?v=\(youtubeKey)") {
            presenter?.updateYoutubeURL(url: url)
        } else {
            
        }
        
        
    }
}
