//
//  Genre.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 12/06/23.
//

import Foundation

struct Genre {
    var name: String
    var id: Int
    
    public init(name: String, id: Int) {
        self.name = name
        self.id = id
    }
}
