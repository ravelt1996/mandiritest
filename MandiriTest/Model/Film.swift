//
//  Film.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//

import Foundation

struct Film {
    var title: String
    var vote_average: Float
    var id: Int
    var original_title: String
    var video: Bool
    var popularity: Float
    var poster_path: String
    var backdrop_path: String
    var vote_count: Int
    var original_language: String
    var overview : String
    var release_date: String
    
    public init(title: String, vote_average: Float, id: Int, original_title: String, video: Bool, popularity: Float, poster_path: String, backdrop_path: String, vote_count: Int, original_language: String, overview: String, release_date: String) {
        self.title = title
        self.vote_average = vote_average
        self.id = id
        self.original_title = original_title
        self.video = video
        self.popularity = popularity
        self.poster_path = "https://image.tmdb.org/t/p/original\(poster_path)"
        self.backdrop_path = "https://image.tmdb.org/t/p/original\(backdrop_path)"
        self.vote_count = vote_count
        self.original_language = original_language
        self.overview = overview
        self.release_date = release_date
    }
}
