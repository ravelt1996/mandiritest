//
//  Review.swift
//  MandiriTest
//
//  Created by Christopher Sonny on 13/06/23.
//

import Foundation

struct Review {
    var url: String
    var author: String
    var content: String
    var created_at: String
    
    public init(url: String, author: String, content: String, created_at: String) {
        self.url = url
        self.author = author
        self.content = content
        self.created_at = created_at
    }
}
